from enum import IntEnum
from functools import partial, reduce
from hashlib import blake2b
from operator import xor
from struct import Struct, calcsize
from zlib import crc32

from .utils import iS


def hashToUint8(h: bytes) -> int:
	res = hashToUint16(h)
	return (res >> 8) ^ (res & 0xFF)


def hashToUint16(h: bytes) -> int:
	res = hashToUint32(h)
	return (res >> 16) ^ (res & 0xFFFF)


def hashToUint32(h: bytes) -> int:
	res = hashToUint64(h)
	return (res >> 32) ^ (res & 0xFFFFFFFF)


def hashToUint64(h: bytes) -> int:
	l = len(h) // 8
	s = Struct("<" + "Q" * l)
	return reduce(xor, s.unpack(h))  # it'd be better to have Divide-and-Conquer reduce for associative functions


def getRoundedHashConverter(sizeInBytes: int):
	if sizeInBytes >= 8:
		return hashToUint64
	elif sizeInBytes >= 4:
		return hashToUint32
	elif sizeInBytes >= 2:
		return hashToUint16

	return hashToUint8


class HashFunctionT(IntEnum):
	blake2b = 0
	blake3 = 1


def getCRC32(src: bytes) -> bytes:
	return iS.pack(crc32(src))


def getHash(func, data: bytes) -> bytes:
	return func(data).digest()


mapEnumToFunc = {
	HashFunctionT.blake2b: partial(getHash, blake2b),
}

try:
	from blake3 import blake3
except ImportError:
	pass
else:
	mapEnumToFunc[HashFunctionT.blake3] = partial(getHash, blake3)
