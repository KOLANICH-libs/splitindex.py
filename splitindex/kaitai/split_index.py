# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

from pkg_resources import parse_version
import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


class SplitIndex(KaitaiStruct):
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.header = SplitIndex.Header(self._io, self, self._root)
        if self.header.flags.has_delimiters:
            self.shape = SplitIndex.ShapeDescriptor(self._io, self, self._root)

        if self.header.flags.has_hash:
            self.hash = SplitIndex.HashDescriptor(self._io, self, self._root)

        self.index = SplitIndex.Index(self.header.level, self._io, self, self._root)

    class Buf(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len = self._io.read_u1()
            self.delimiter = self._io.read_bytes(self.len)


    class Header(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.signature = self._io.read_bytes(8)
            if not self.signature == b"\x73\x70\x6C\x74\x69\x64\x78\x0A":
                raise kaitaistruct.ValidationNotEqualError(b"\x73\x70\x6C\x74\x69\x64\x78\x0A", self.signature, self._io, u"/types/header/seq/0")
            self.version = self._io.read_u1()
            self.level = self._io.read_u1()
            self.flags = SplitIndex.Header.Flags(self._io, self, self._root)

        class Flags(KaitaiStruct):

            class SplitMetainfo(Enum):
                none = 0
                shape_only = 1
                shape_and_delimiters = 2
                reserved3 = 3
            def __init__(self, _io, _parent=None, _root=None):
                self._io = _io
                self._parent = _parent
                self._root = _root if _root else self
                self._read()

            def _read(self):
                self.split_metainfo = KaitaiStream.resolve_enum(SplitIndex.Header.Flags.SplitMetainfo, self._io.read_bits_int_le(2))
                self.has_hash = self._io.read_bits_int_le(1) != 0
                self.reserved0 = self._io.read_bits_int_le(5)
                self._io.align_to_byte()
                self.reserved1 = self._io.read_u2le()
                self.reserved2 = self._io.read_u1()

            @property
            def has_shape(self):
                if hasattr(self, '_m_has_shape'):
                    return self._m_has_shape if hasattr(self, '_m_has_shape') else None

                self._m_has_shape = self.split_metainfo.value >= SplitIndex.Header.Flags.SplitMetainfo.shape_only.value
                return self._m_has_shape if hasattr(self, '_m_has_shape') else None

            @property
            def has_delimiters(self):
                if hasattr(self, '_m_has_delimiters'):
                    return self._m_has_delimiters if hasattr(self, '_m_has_delimiters') else None

                self._m_has_delimiters = self.split_metainfo.value >= SplitIndex.Header.Flags.SplitMetainfo.shape_and_delimiters.value
                return self._m_has_delimiters if hasattr(self, '_m_has_delimiters') else None



    class HashDescriptor(KaitaiStruct):
        """stores info about strong cryptographic hash of a file."""

        class HashFunction(Enum):
            blake2b = 0
            blake3 = 1
            sha3_512 = 2
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.hash_function = KaitaiStream.resolve_enum(SplitIndex.HashDescriptor.HashFunction, self._io.read_u1())
            self.hash = self._io.read_bytes(self.size)

        @property
        def size(self):
            if hasattr(self, '_m_size'):
                return self._m_size if hasattr(self, '_m_size') else None

            self._m_size = (64 if  ((self.hash_function == SplitIndex.HashDescriptor.HashFunction.blake2b) or (self.hash_function == SplitIndex.HashDescriptor.HashFunction.blake2b) or (self.hash_function == SplitIndex.HashDescriptor.HashFunction.sha3_512))  else 0)
            return self._m_size if hasattr(self, '_m_size') else None


    class ShapeDescriptor(KaitaiStruct):
        """Stores the info about the way the file was generated from Comma,Tab Separated Values files.
        [TC]SV usually have `size` of 2, `shape` equal `(rows, records in a row)` and `delimiters` ("\n", ",") or ("\n", "\t")
        Uses:
          * acceleration of [CT]SV parsing
          * weak probabilistic integrity checking.
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.size = self._io.read_u1()
            if not self.size >= 1:
                raise kaitaistruct.ValidationLessThanError(1, self.size, self._io, u"/types/shape_descriptor/seq/0")
            self.shape = self._io.read_bytes(self.size)
            if self._parent.header.flags.has_shape:
                self.delimiters = [None] * (self.size)
                for i in range(self.size):
                    self.delimiters[i] = SplitIndex.Buf(self._io, self, self._root)




    class Index(KaitaiStruct):
        def __init__(self, level, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self.level = level
            self._read()

        def _read(self):
            self.prefix = self._io.read_u1()
            self.count_minus_1 = self._io.read_u1()
            self.subindexes = [None] * (self.count)
            for i in range(self.count):
                _on = self.is_leaf
                if _on == True:
                    self.subindexes[i] = self._io.read_u1()
                elif _on == False:
                    self.subindexes[i] = SplitIndex.Index((self.level - 1), self._io, self, self._root)


        @property
        def count(self):
            if hasattr(self, '_m_count'):
                return self._m_count if hasattr(self, '_m_count') else None

            self._m_count = (self.count_minus_1 + 1)
            return self._m_count if hasattr(self, '_m_count') else None

        @property
        def is_leaf(self):
            if hasattr(self, '_m_is_leaf'):
                return self._m_is_leaf if hasattr(self, '_m_is_leaf') else None

            self._m_is_leaf = self.level == 0
            return self._m_is_leaf if hasattr(self, '_m_is_leaf') else None



