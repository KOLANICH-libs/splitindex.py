from struct import Struct
import typing
import math
import array

s = Struct(">Q")

iS = Struct("<I")


# from bisect1 import bisect_right as binSearch


def binSearch(arr: list, v: int, key: typing.Callable) -> int:
	if not arr:
		return -1

	b = len(arr)
	a = 0

	d = b - a
	while d:
		m = (a + b) // 2
		el = key(arr[m])
		if el >= v:  # [ a v el b ]
			b = m
		else:
			a = m + 1
		d = b - a
	return a


def encodeNum(n: int):
	return tuple(s.pack(n))


def recursiveDefaultDict():
	return defaultdict(recursiveDefaultDict)

def numberSizeInBytes(n: int) -> int:
	return math.ceil(n.bit_length() / 8)


class BitSerializer:
	__slots__ = ("chunkSize", "bytesCounter", "bitsCounter", "currentByte")

	def __init__(self, chunkSize: int):
		self.chunkSize = chunkSize
		self.reset()

	def reset(self):
		self.bytesCounter = 0
		self.bitsCounter = 0
		self.currentByte = 0

	def advance(self) -> int:
		res = self.currentByte
		self.currentByte = 0
		self.bytesCounter += 1
		self.bitsCounter = 0
		return res

	def serializeChunk(self, chunk: int) -> typing.Iterator[int]:
		#print("chunk", bin(chunk))
		spaceLeftInTheCurrentByte = 8 - self.bitsCounter

		incr = self.chunkSize
		#print("spaceLeftInTheCurrentByte", spaceLeftInTheCurrentByte)

		
		if incr > spaceLeftInTheCurrentByte:
			upperPartSize = spaceLeftInTheCurrentByte
			lowerPartSize = self.chunkSize - upperPartSize

			#print("self.bitsCounter", self.bitsCounter)
			#print("upperPartSize", upperPartSize)
			#print("lowerPartSize", lowerPartSize)

			lowerPartMask = (1 << lowerPartSize) - 1
			upperPartMask = (1 << upperPartSize) - 1
			#print(bin(chunk), bin(upperPartMask << upperPartSize), upperPartSize, bin(chunk >> upperPartSize))
			upperPart = (chunk >> lowerPartSize) & upperPartMask
			lowerPart = chunk & lowerPartMask
			#print("upperPart", bin(upperPart))
			#print("lowerPart", bin(lowerPart))
			chunk = lowerPart

			self.currentByte |= upperPart << (8 - self.bitsCounter - upperPartSize);
			#print("1 self.currentByte", bin(self.currentByte))
			incr -= spaceLeftInTheCurrentByte
			yield self.advance()
		else:
			lowerPartSize = self.chunkSize

		self.currentByte |= chunk << (8 - self.bitsCounter - lowerPartSize)
		#print("0 self.currentByte", bin(self.currentByte))
		self.bitsCounter += incr

	def finalize(self):
		if self.bitsCounter:
			yield self.advance()

	def __call__(self, chunks: typing.Iterable[int]) -> typing.Iterator[int]:
		self.reset()
		for chunk in chunks:
			yield from self.serializeChunk(chunk)

		yield from self.finalize()

