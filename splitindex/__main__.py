import sys
from pathlib import Path

from fsutilz import MMap
from plumbum import cli

from . import _findIndices, encodeIndices, serializeFormat


class MainCLI(cli.Application):
	"""SplitIndex is a spec to describe indexes of files made of densily-packed records.
	This is a tool allowing you to manipulate such specs in some easy cases. In more complicated cases build your own app based on the lib.
	"""


sepsPresets = {
	"csv": ["\n", ","],
	"tsv": ["\n", "\t"],
}
defaultSep = ("\n",)


@MainCLI.subcommand("index")
class IndexCLI(cli.Application):
	"""This command allows you create an index for a file"""

	sep = cli.SwitchAttr(["-s", "--sep"], str, default=None, help="Separator spec encoded as JSON.")

	def main(self, file: cli.ExistingFile):
		inpFile = Path(file)

		if self.sep is None:
			ext = inpFile.suffix
			if ext and ext[0] == ".":
				ext = ext[1:].lower()
				sep = sepsPresets.get(ext, defaultSep)
			else:
				sep = defaultSep
		else:
			import json

			sep = tuple(json.loads(self.sep))

		print(repr(sep))
		with MMap(inpFile) as m:
			indices = _findIndices(m, defaultSep)
		encoded = encodeIndices(indices)
		outFile = inpFile.parent / (inpFile.name + ".splitIndex")
		outFile.write_bytes(b"".join(serializeFormat(encoded)))


if __name__ == "__main__":
	MainCLI.run()
