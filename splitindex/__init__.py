import typing
from collections import defaultdict
from enum import IntEnum

from fsutilz import MMap

from .kaitai.split_index import SplitIndex
from .utils import iS, s


class SplitMetaInfo(IntEnum):
	none = 0
	shape = 1
	shape_and_delimiters = 2


IndicesT = typing.Iterable[int]


def _findIndices(buffer: bytes, c: int) -> IndicesT:
	return [i for i, b in enumerate(buffer) if b == c]


CodeT = typing.Tuple[int, int, int, int, int, int, int, int]


def encodeNum(n: int) -> CodeT:
	return tuple(s.pack(n))


def recursiveDefaultDict():
	return defaultdict(recursiveDefaultDict)


def genSeq_(indices: IndicesT) -> typing.Iterator[CodeT]:
	for index in indices:
		yield encodeNum(index)


def genSeq(indices):
	return list(genSeq_(indices))


EncodedT = typing.Iterable[typing.Union[typing.Tuple[int, "EncodedT"], int]]
EncodedUnlazyT = typing.Tuple[typing.Union[typing.Tuple[int, "EncodedUnlazyT"], int]]


def processSeq_(encoded: typing.Iterable[CodeT], depth: int = 0) -> EncodedT:
	if depth < 7:
		accum = [None] * 256
		for el in encoded:
			k = el[0]
			v = el[1:]
			c = accum[k]
			if c is None:
				accum[k] = c = []
			c.append(v)

		res = []
		for i, v in enumerate(accum):
			if v is not None:
				yield (i, processSeq_(v, depth + 1))
	else:
		for el in encoded:
			yield from el


def encodeIndices_(seq: IndicesT) -> EncodedT:
	return processSeq_(genSeq_(seq))


def unlazyEncoded(encoded: EncodedT, depth: int = 0) -> EncodedUnlazyT:
	if depth < 7:
		res = []
		for pref, e in encoded:
			res.append((pref, tuple(unlazyEncoded(e, depth + 1))))
		return tuple(res)
	else:
		return tuple(encoded)


def encodeIndices(seq: IndicesT) -> EncodedT:
	return unlazyEncoded(encodeIndices_(seq))


def decodeIndices_(encoded: EncodedT, prefix, depth: int = 0):
	if depth < 7:
		for pref, e in encoded:
			yield from decodeIndices_(e, (prefix << 8) | pref, depth + 1)
	else:
		for e in encoded:
			yield (prefix << 8) | e


def decodeIndices(encoded, prefix=0):
	return list(decodeIndices_(encoded, prefix))


def parseFormat(b):
	s = Splitindex.from_bytes(b)
	yield from parseIndex(s.idx)


def parseIndex(idx, prefix=0):
	res = []
	for el in idx.subindexes:
		if isinstance(el, int):
			yield prefix << 8 | el
		else:
			yield from parseIndex(el, prefix << 8 | el.prefix)


def serializeFormat(trie, shape=None, delimiters=None, fullHash: bytes = None, randomHash: bytes = None):
	splitMetainfo = SplitMetaInfo.none  # type: SplitMetaInfo
	if shape is not None:
		if delimiters:
			splitMetainfo = SplitMetaInfo.shape_and_delimiters
		else:
			splitMetainfo = SplitMetaInfo.shape_only
	else:
		if delimiters:
			splitMetainfo = SplitMetaInfo.shape_only
			shape = (0,) * len(delimiters)

	yield b"".join(serializeHeader(version=0, level=7, splitMetainfo=splitMetainfo, hasFullHash=fullHash is not None, hasRandomHash=randomHash is not None))
	if shape is not None:
		yield from serializeShapeDescriptor(shape, delimiters)

	yield bytes(trie.serialize())


def serializeShapeDescriptor(shape, delimiters):
	if delimiters is not None:
		if len(shape) != len(delimiters):
			raise ValueError("lengths of `shape` and `delimiters` must match!", len(shape), len(delimiters))

	yield bytes((len(shape),))
	yield bytes(shape)

	if delimiters is not None:
		for d in delimiters:
			yield bytes((len(d),))
			yield d


def serializeFlagsInt(splitMetainfo: SplitMetaInfo, hasFullHash: bool, hasRandomHash: bool):
	return int(splitMetainfo) | (hasFullHash << 2) | (hasRandomHash << 3)


def serializeFlags(splitMetainfo: SplitMetaInfo, hasFullHash: bool, hasRandomHash: bool) -> bool:
	return iS.pack(serializeFlagsInt(splitMetainfo, hasFullHash, hasRandomHash))


serializeFlags.__wraps__ = serializeFlagsInt


def serializeHeader(version, level, splitMetainfo: SplitMetaInfo, hasFullHash: bool, hasRandomHash: bool):
	yield b"spltidx\n"
	yield bytes((version, level))
	yield serializeFlags(splitMetainfo, hasFullHash, hasRandomHash)


def serializeIndex(level, prefix, subindexes):
	# yield level
	yield prefix
	subindexes = list(subindexes)
	yield len(subindexes) - 1
	if level != 7:
		for p, v in subindexes:
			yield from serializeIndex(level + 1, p, v)
	else:
		yield from subindexes
