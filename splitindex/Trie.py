from io import StringIO

import matplotlib
import pygraphviz as gv
from .utils import binSearch, numberSizeInBytes, encodeNum, BitSerializer
import math
import typing
import array

from itertools import accumulate

from struct import Struct

qS = Struct("<Q")


class NodeBase:
	__slots__ = ("i", "v")

	def __init__(self, i: int, v: int) -> None:
		self.i = i
		self.v = v

	def serialize(self, level, maxLevel):
		yield self.v


class SortedArr:
	__slots__ = ("a",)

	def __init__(self) -> None:
		self.a = []

	def __len__(self) -> int:
		return len(self.a)

	def __iter__(self):
		return iter(self.a)

	def getByI(self, k: int) -> NodeBase:
		if not self.a:
			return None
		idx = binSearch(self.a, k, key=lambda x: x.i)
		if len(self.a) <= idx:
			return None
		r = self.a[idx]
		if r.i == k:
			return r
		return None

	def setByI(self, k: int, v: NodeBase) -> NodeBase:
		if not self.a:
			self.a.append(v)
			return

		idx = binSearch(self.a, k, key=lambda x: x.i)
		self.a.insert(idx, v)

	def getByV(self, k: int) -> NodeBase:
		if not self.a:
			return None
		idx = binSearch(self.a, k, key=lambda x: x.v)
		if len(self.a) <= idx:
			return None
		r = self.a[idx]
		if r.v == k:
			return r
		return None

	def setByV(self, k: int, v: NodeBase) -> NodeBase:
		if not self.a:
			self.a.append(v)
			return

		idx = binSearch(self.a, k, key=lambda x: x.v)
		self.a.insert(idx, v)


def getGVString(g) -> str:
	import networkx as nx
	import networkx.drawing

	with StringIO() as s:
		nx.drawing.nx_pydot.write_dot(g, s)
		return s.getvalue()


def showGraph(g) -> "IPython.display.SVG":
	from IPython.display import SVG

	gg = gv.AGraph()
	s = getGVString(g)
	# print(s)
	gg.from_string(s)
	gg.layout("dot")
	return SVG(data=gg.draw(None, "svg"))


class Leaf(NodeBase):
	__slots__ = ("level",)

	def __init__(self, i: int, level: int, v: int) -> None:
		super().__init__(i, v)
		self.level = level

	def __repr__(self):
		return hex(self.v)[2:] + "(l=" + str(self.level) + ", i=" + str(self.i) + ")"


class Node(NodeBase):
	__slots__ = ("level", "count", "children")

	def __init__(self, i: int, level: int, v: int) -> None:
		super().__init__(i, v)
		self.level = level
		self.count = 0
		self.children = SortedArr()

	def __len__(self) -> int:
		return len(self.children)

	def __iter__(self):
		return iter(self.children)

	def get(self, codeComp: int) -> typing.Optional["Node"]:
		return self.children.getByV(codeComp)

	def set(self, codeComp: int, v: NodeBase) -> None:
		return self.children.setByV(codeComp, v)

	def setSeq(self, codeComp: int, v: NodeBase) -> None:
		return self.children.a.append(v)

	def __repr__(self) -> str:
		return hex(self.v)[2:] + "(l=" + str(self.level) + ", ct=" + str(self.count) + ", #c=" + str(len(self.children)) + ", i=" + str(self.i) + ")"

	def addLeaf(self, codePart, iD, level, idx):
		self.children[codePart] = Leaf(iD, level, idx)

	def serializeChildren(self, level, maxLevel):
		yield len(self.children) - 1
		if level != maxLevel:
			sizes = [v.count for v in self.children]
			if False:
				if(sizes[-1].bit_length() > 8):
					sizeSize = numberSizeInBytes(sizes[-1])
					for s in sizes:
						yield from qS.pack(s)[:sizeSize]
					#print(sizes)
					#offsets = list(accumulate(sizes))
					#w1 = Walker(offsets)
					#w1()
					#yield from w1.t.serialize()
				else:
					bs = BitSerializer(sizes[-1].bit_length())
					for s in bs(sizes):
						yield s

		for v in self.children:
			yield from v.serialize(level + 1, maxLevel)

	def serialize(self, level, maxLevel):
		assert level == self.level
		#yield level
		yield from super().serialize(level, maxLevel)
		#yield self.count
		yield from self.serializeChildren(level, maxLevel)


def toNx(n, g=None) -> "networkx.DiGraph":
	import networkx as nx

	if g is None:
		g = nx.DiGraph()

	g.add_node(n.i, label=repr(n))

	if isinstance(n, Node):
		for c in n.children.a:
			# g.add_node(c.i, label=repr(c))
			toNx(c, g)
			# print(n, n.i, c.i)
			g.add_edge(n.i, c.i, label=hex(c.v))
	else:
		g.nodes[n.i]["shape"] = "house"

	return g


rootLevel = 0

def prettyCode(n: int, maxLen: int) -> str:
	return "(" + ", ".join(map(lambda e: hex(e)[2:], encodeNum(n)[-maxLen:])) + ")"



class Trie:
	__slots__ = ("bits", "mask", "root", "iD", "debug", "maxLevel", "level", "i", "n")

	def __init__(self, maxLevel: int, bits: int = 8) -> None:
		self.bits = bits
		self.mask = (1 << bits) - 1
		self.iD = -1
		self.root = self.makeNonLeaf(-1, rootLevel, -1)
		self.debug = False
		self.maxLevel = maxLevel

		self.level = rootLevel
		self.i = -1
		self.n = self.root

	def gcp(self, num: int, level: int) -> int:
		"""Get code part"""
		idx = self.maxLevel - level
		if self.debug:
			print("gcp", "code=", prettyCode(num, self.maxLevel), "level=", level, "idx=", idx)
		assert idx >= 0, "idx < 0"
		assert idx <= self.maxLevel, "idx > maxLevel"
		return (num >> (idx * self.bits)) & self.mask

	def toNx(self) -> "networkx.DiGraph":
		return toNx(self.root)

	def makeNonLeaf(self, codePart: int, level: int, offs: int) -> Node:
		res = Node(self.iD, level, codePart)
		self.iD += 1
		return res

	def makeLeaf(self, offs: int) -> Leaf:
		codePart = self.gcp(offs, self.maxLevel)
		res = Leaf(self.iD, self.maxLevel, codePart)
		self.iD += 1
		return res

	def findParent(self, offs: int, inc: int = 0) -> NodeBase:
		if self.debug:
			print("Descending")
		current = self.root
		newCurrent = None
		level = rootLevel
		current.count += inc

		while True:
			codePart = self.gcp(offs, level)
			# print(current, "codePart", hex(codePart))
			newCurrent = current.get(codePart)
			if self.debug:
				print(current, "codePart", hex(codePart), "newCurrent", newCurrent)
			if not newCurrent:
				if self.debug:
					print("Descended", current, level)
				return (current, level)

			level += 1
			current = newCurrent
			current.count += inc

	def toGraphSVG(self):
		return showGraph(self.toNx())

	def show(self) -> None:
		from IPython.display import display

		display(self.toGraphSVG())

	def ascendFromNode(self, n: Leaf, offs: int, targetLevel: int, sequential: bool = False) -> NodeBase:
		if self.debug:
			print("Ascending")

		level = n.level

		if self.debug:
			print("level", level, "targetLevel", targetLevel)

		upperLevel = level - 1
		while level > targetLevel:
			if self.debug:
				print("level", level)
			n1 = self.makeNonLeaf(self.gcp(offs, upperLevel), level, offs)
			n1.count += 1
			codePart = self.gcp(offs, level)
			if sequential:
				n1.setSeq(en)
			else:
				n1.set(codePart, n)
			if self.debug:
				print("codePart=", hex(codePart), "level=", level, "n1=", n1)
			n = n1
			level = upperLevel
			upperLevel = level - 1

		if self.debug:
			print("Ascended", n)
		return n

	def insertLeaf(self, offs: int, sequential: bool = False) -> None:
		parent, level = self.findParent(offs, 1)
		if self.debug:
			print(parent, level)
		codePart = self.gcp(offs, level)
		l = self.makeLeaf(offs)
		if self.debug:
			print("Leaf: ", l)
		subtree = self.ascendFromNode(l, offs, level, sequential)
		if sequential:
			parent.setSeq(subtree)
		else:
			parent.set(codePart, subtree)

		# nn.children[codePart] = l

	def lookUpByIndex(self, idx: int, n = None):
		origIdx = idx
		if n is None:
			n = self.root

		while n.level < self.maxLevel:
			found = False
			cumLen = 0
			for el in n.children.a:
				cumLenNext = cumLen + el.count
				if cumLenNext > idx:
					n = el
					idx -= cumLen
					found = True
					break
				cumLen = cumLenNext
			if not found:
				raise KeyError("Not found", origIdx, idx, n.level, cumLenNext)

		return n.children.a[idx]

	def serialize(self):
		return self.root.serializeChildren(0, self.maxLevel)


class Walker:
	__slots__ = ("offs", "idx", "i", "t", "n")

	def __init__(self, offs: typing.Iterable[int], bits: int = 8) -> None:
		self.offs = offs
		self.idx = None
		self.i = 0
		maxLevel = math.ceil(offs[-1].bit_length() / bits)
		self.t = Trie(maxLevel, bits=bits)
		self.n = self.t.root

	def insertLeaf(self, sequential: bool = False) -> None:
		idx = self.offs[self.i]
		self.idx = idx
		self.t.insertLeaf(self.idx, sequential = sequential)
		self.i += 1

	@property
	def hasNext(self) -> bool:
		return self.i >= len(self.offs)

	def __next__(self) -> bool:
		if self.hasNext:
			return False

		# print(self.i, hex(self.idx))
		self.n = self.insertLeaf()

		return True

	def __call__(self) -> None:
		shouldIter = True
		while shouldIter:
			shouldIter = next(self)

		return self.n
