meta:
  id: split_index
  endian: le
  bit-endian: le

seq:
  - id: header
    type: header
  - id: shape
    type: shape_descriptor
    if: header.flags.has_delimiters
  - id: hash
    type: hash_descriptor
    if: header.flags.has_hash
  - id: index
    type: index(header.level)

types:
  buf:
    seq:
      - id: len
        type: u1
      - id: delimiter
        size: len

  shape_descriptor:
    doc: |
      Stores the info about the way the file was generated from Comma,Tab Separated Values files.
      [TC]SV usually have `size` of 2, `shape` equal `(rows, records in a row)` and `delimiters` ("\n", ",") or ("\n", "\t")
      Uses:
        * acceleration of [CT]SV parsing
        * weak probabilistic integrity checking.
    seq:
      - id: size
        type: u1
        valid:
          min: 1
      - id: shape
        size: size
      - id: delimiters
        doc: A delimiter can be an empty buf! This means there is no delimiter and recors follow each other immediately.
        type: buf
        repeat: expr
        repeat-expr: size
        if: _parent.header.flags.has_shape

  hash_descriptor:
    doc: stores info about strong cryptographic hash of a file
    seq:
      - id: hash_function
        type: u1
        enum: hash_function
      - id: hash
        size: size
    instances:
      size:
        value: "hash_function == hash_function::blake2b or hash_function == hash_function::blake2b or hash_function == hash_function::sha3_512 ? 64 : 0"
    enums:
      # ONLY CRYPTOGRAPHIC HASH FUNCTIONS INHERENTLY IMMUNE TO LENGTH EXTENSION ARE ALLOWED HERE
      hash_function:
        0: blake2b
        1: blake3
        2: sha3_512

  header:
    seq:
      - id: signature
        contents: "spltidx\n"
      - id: version
        type: u1
      - id: level
        type: u1
      - id: flags
        type: flags
    types:
      flags:
        seq:
          - id: split_metainfo
            type: b2
            enum: split_metainfo
          - id: has_hash
            type: b1
          - id: reserved0
            type: b5
          - id: reserved1
            type: u2
          - id: reserved2
            type: u1
        instances:
          has_shape:
            value: split_metainfo.to_i >= split_metainfo::shape_only.to_i
          has_delimiters:
            value: split_metainfo.to_i >= split_metainfo::shape_and_delimiters.to_i
        enums:
          split_metainfo:
            0: none
            1: shape_only
            2: shape_and_delimiters
            3: reserved3

  index:
    params:
      - id: level
        type: u1
    seq:
      - id: prefix
        type: u1
      - id: count_minus_1
        type: u1
      - id: subindexes
        type:
          switch-on: is_leaf
          cases:
            true: u1
            false: index(level - 1)
        repeat: expr
        repeat-expr: count
    instances:
      count:
        value: count_minus_1 + 1
      is_leaf:
        value: level == 0
