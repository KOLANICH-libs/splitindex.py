CMAKE_MINIMUM_REQUIRED(VERSION 3.14)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake")

include(CheckSymbolExists)
include(CheckIncludeFile)
include(CheckIncludeFileCXX)

find_package(Python COMPONENTS Interpreter)

PROJECT(ScanBytes)

find_package(HydrArgs)
find_package(HydrArgs_discoverer)

set(CMAKE_USE_RELATIVE_PATHS TRUE)

set(PACKAGE_NAME "ScanBytes" CACHE STRING "Program name")
set(PACKAGE_VERSION ${VERSION})
set(PACKAGE_STRING "${PACKAGE_NAME} ${PACKAGE_VERSION}")

set(Include_dir "${CMAKE_CURRENT_SOURCE_DIR}/include")
set(Source_dir "${CMAKE_CURRENT_SOURCE_DIR}/src")
set(CMake_Misc_Dir "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

# config.h

file(GLOB_RECURSE SRCFILES "${Source_dir}/*.c" "${Source_dir}/*.cpp")
file(GLOB_RECURSE resource_files "${Source_dir}/*.rc" "${Source_dir}/*.ico")
if(MSVC_IDE)
	file(GLOB_RECURSE HDRFILES "${Source_dir}/*.h" "${Source_dir}/*.hpp" "${Include_dir}/*.h" "${Include_dir}/*.hpp")
endif()

message(STATUS "${SRCFILES} ${HDRFILES} ${resource_files}")
add_executable(ScanBytes "${SRCFILES}" "${HDRFILES}" "${resource_files}")
target_include_directories(ScanBytes PUBLIC "${Include_dir}")

target_link_libraries(ScanBytes PRIVATE HydrArgs::HydrArgs HydrArgs_discoverer::HydrArgs_discoverer)

target_compile_definitions(ScanBytes PUBLIC "-D_UNICODE=1" "-DUNICODE=1")

target_compile_options(ScanBytes PRIVATE "-Wall")

set_property(TARGET ScanBytes PROPERTY CXX_STANDARD 20)
set_property(TARGET ScanBytes PROPERTY PREFIX "")
#harden(ScanBytes)

if(MSVC_IDE)
	source_group("res" FILES ${resource_files})
endif()
