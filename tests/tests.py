#!/usr/bin/env python3
import sys
from pathlib import Path
import unittest
import itertools

thisDir = Path(__file__).parent

sys.path.insert(0, str(thisDir.parent))

from collections import OrderedDict

dict = OrderedDict

import splitindex
from splitindex.utils import BitSerializer


class Tests(unittest.TestCase):

	def testBitSerializer(self):
		matrix = {
			2: (0b00_01_10_11,),
			3: (0b000_001_01, 0b0_011_100_1, 0b01_110_111),
			4: (0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef),
			5: (
				0b00000_000, 0b01_00010_0, 0b0011_0010, 0b0_00101_00, 0b110_00111,
				0b01000_010, 0b01_01010_0, 0b1011_0110, 0b0_01101_01, 0b110_01111,
				0b10000_100, 0b01_10010_1, 0b0011_1010, 0b0_10101_10, 0b110_10111,
				0b11000_110, 0b01_11010_1, 0b1011_1110, 0b0_11101_11, 0b110_11111
			)
		}

		for bitness, expected in matrix.items():
			with self.subTest(bitness=bitness):
				s = BitSerializer(bitness)
				size = 1 << bitness

				actual = tuple(s(range(size)))
				self.assertEqual(actual, expected)
		

	@unittest.skip
	def testFindIndices(self):
		self.assertEqual(tuple(splitindex.findIndices(b"a\nbc\ndef\n", ord("\n"))), (1, 4, 8))

	def testUnlazingEncoded(self):
		etalon = ((0, ((0, ((0, ((0, ((0, ((0, ((0, (1, 2)), (1, (0,)))), (1, ((35, (69,)),)))),)),)),)),)),)
		res = splitindex.unlazyEncoded(etalon)
		self.assertEqual(etalon, res)

	def testEncodeIndices(self):
		self.assertEqual(splitindex.encodeIndices([1, 2, 0x10, 0x100, 0x200, 0x12345]), ((0, ((0, ((0, ((0, ((0, ((0, ((0, (1, 2, 16)), (1, (0,)), (2, (0,)))), (1, ((35, (69,)),)))),)),)),)),)),))

if __name__ == "__main__":
	unittest.main()
