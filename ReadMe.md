splitindex.py [![Unlicensed work](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/)
=============
[wheel (GHA via `nightly.link`)](https://nightly.link/KOLANICH-libs/splitindex.py/workflows/CI/master/splitindex-0.CI-py3-none-any.whl)
[![GitHub Actions](https://github.com/KOLANICH-libs/splitindex.py/workflows/CI/badge.svg)](https://github.com/KOLANICH-libs/splitindex.py/actions/)
[![Libraries.io Status](https://img.shields.io/librariesio/github/KOLANICH-libs/splitindex.py.svg)](https://libraries.io/github/KOLANICH-libs/splitindex.py)

SplitIndex is an index for files **consisting** of a **stream of consecutive** records of **variable size**. It allows you to get a record fast by its index without converting the whole data into your own format (which can double space consumed, because you would likely need both files).

Examples of such files are:
	* just text files containing something in their lines;
	* something-separated-values, such as CSV and TSV;
	* binary files that are streams of records untill the end of file happens.
